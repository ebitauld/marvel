import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {Personnage} from '../interfaces/personnage';
import {map} from 'rxjs/operators';

@Injectable()

export class PersonnagesService {

    apiUrl:string;
    publicKey:string;
    privateKey:string;
    ts:string;
    hash:string;
    offset:number;
    limit:number;

    personnagesList:any[] = [];
    favrorisList:any[] = [];

    constructor(private http:HttpClient) {
        this.apiUrl = 'https://gateway.marvel.com/v1/public/characters';
        this.ts = '1';
        this.privateKey = '973642545c65df7b66462630db3ac884074df405';
        this.publicKey = '3ab0ab7d61a72da0006f2a68e33a8ca1';
        this.hash = 'fd7bc4c0153debbb273a9d00b1bc9e63';
        this.offset = -20;
        this.limit = 20;

        /*ts - a timestamp (or other long string which can change on a request-by-request basis)*/
        /*hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)*/
        /*For example, a user with
         a public key of "1234"
         a private key of "abcd"
         could construct a valid call as follows: http://gateway.marvel.com/v1/public/comics?ts=1&apikey=1234&hash=ffd275c5130566a2916217b101f26150 (the hash value is the md5 digest of 1abcd1234)*/
    }

    public addItem(item) {
        let itemExist = false;
        this.personnagesList.forEach((obj, index) => {
            if (item.id === obj.id) {
                itemExist = true;
            }
        });
        if (!itemExist) {
            this.personnagesList.push({
                id: item.id,
                name: item.name,
                thumbnail: item.thumbnail,
                isFavorite: this.favrorisList.some(e => e.id === item.id)
            });
        }
    }

    public toggleFavorite(item) {
        if (this.favrorisList.some(e => e.id === item.id)) {
            this.favrorisList.forEach((obj, index) => {
                if (item.id === obj.id) {
                    obj.isFavorite = !obj.isFavorite;
                    this.favrorisList.splice(index, 1);
                }
            });
        } else {
            item.isFavorite = true;
            this.favrorisList.push(item);
        }
        localStorage.setItem('favrorisList', JSON.stringify(this.favrorisList));
    }

    public getFavorisList() {
        this.favrorisList = JSON.parse(localStorage.getItem('favrorisList'));
        return this.favrorisList;
    }

    requestList():Observable<Personnage[]> {
        this.offset = this.offset + this.limit;
        console.info("offset", this.offset);
        if (this.offset === 0) {
            this.getFavorisList();
            console.info("fav", this.favrorisList);
        }

        return this.http.get(this.apiUrl + "?apikey=" + this.publicKey + '&ts=' + this.ts + '&hash=' + this.hash + '&offset=' + this.offset + '&limit=' + this.limit).pipe(
            map((res:Personnage[]) => {
                    if (typeof res['data'] !== 'undefined' && typeof res['data']['results'] !== 'undefined') {
                        res['data']['results'].forEach((obj, index) => {
                            this.addItem(obj);
                        });
                    }

                    return this.personnagesList;
                }
            )
        )
    }
}

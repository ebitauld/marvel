import {BrowserModule} from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxInfiniteScrollerModule} from 'ngx-infinite-scroller';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

import {FontAwesomeModule, FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';

import {HeaderComponent} from './partials/header/header.component';
import {PersonnagesComponent} from './pages/personnages/personnages.component';
import {FavorisComponent} from './pages/favoris/favoris.component';
import {HomeComponent} from './pages/home/home.component';
import {PersonnageComponent} from './ressources/personnage/personnage.component';

import {PersonnagesService} from './services/personnages.service';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        PersonnagesComponent,
        FavorisComponent,
        HomeComponent,
        PersonnageComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        MatToolbarModule, MatMenuModule, MatIconModule, MatButtonModule,
        FontAwesomeModule,
        NgxInfiniteScrollerModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [PersonnagesService],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(library:FaIconLibrary) {
        library.addIconPacks(fas);
    }
}

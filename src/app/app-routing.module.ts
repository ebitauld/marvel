import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {PersonnagesComponent} from './pages/personnages/personnages.component';
import {FavorisComponent} from './pages/favoris/favoris.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'personnages', component: PersonnagesComponent },
  { path: 'favoris', component: FavorisComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
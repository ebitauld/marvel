export interface Personnage {

    id:number; /* (int, optional): The unique ID of the character resource.,*/
    name:string; /*(string, optional): The name of the character.,*/
    description:string; /* (string, optional): A short bio or description of the character.,*/
    modified:any; /* (Date, optional): The date the resource was most recently modified.,*/
    resourceURI:string; /* (string, optional): The canonical URL identifier for this resource.,*/
    urls:any; /* (Array[Url], optional): A set of public web site URLs for the resource.,*/
    thumbnail:any; /* (Image, optional): The representative image for this character.,*/
    comics:any; /* (ComicList, optional): A resource list containing comics which feature this character.,*/
    stories:any; /* (StoryList, optional): A resource list of stories in which this character appears.,*/
    events:any; /* (EventList, optional): A resource list of events in which this character appears.,*/
    series:any; /* (SeriesList, optional): A resource list of series in which this character appears.*/
}
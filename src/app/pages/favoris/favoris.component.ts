import {Component, OnInit} from '@angular/core';
import {PersonnagesService} from '../../services/personnages.service';

import {Personnage} from '../../interfaces/personnage';

@Component({
    selector: 'app-favoris',
    templateUrl: './favoris.component.html',
    styleUrls: ['./favoris.component.scss']
})
export class FavorisComponent implements OnInit {

    favList:Personnage[];

    constructor(private personnagesService:PersonnagesService) {
    }

    ngOnInit():void {
        this.favList = this.personnagesService.getFavorisList();
    }

}

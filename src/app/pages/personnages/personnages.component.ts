import {Component, OnInit} from '@angular/core';
import {PersonnagesService} from '../../services/personnages.service';

import {Personnage} from '../../interfaces/personnage';

@Component({
    selector: 'app-personnages',
    templateUrl: './personnages.component.html',
    styleUrls: ['./personnages.component.scss']
})
export class PersonnagesComponent implements OnInit {

    persoList:Personnage[];
    isLoading: Boolean = false;

    constructor(private personnagesService: PersonnagesService) {}

    ngOnInit():void {
        this.addItem();
    }

    public onScrollDown(): void {
        this.addItem();
    }

    addItem():void {
        this.isLoading = true;
        this.personnagesService.requestList().subscribe((res: Personnage[]) => {
            this.persoList = res;
            this.isLoading = false;
        })
    }
}

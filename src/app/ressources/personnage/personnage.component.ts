import {Component, OnInit, Input} from '@angular/core';
import {PersonnagesService} from '../../services/personnages.service';

@Component({
    selector: 'app-personnage',
    templateUrl: './personnage.component.html',
    styleUrls: ['./personnage.component.scss']
})
export class PersonnageComponent implements OnInit {

    @Input() data:any;

    constructor(private personnagesService: PersonnagesService) {

    }

    ngOnInit():void {
    }

    toggleFavorite() {
        this.personnagesService.toggleFavorite(this.data);
    }
}
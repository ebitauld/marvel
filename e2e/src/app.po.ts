import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getHomeTitle(): Promise<string> {
    return element(by.css('app-root .content .home-title')).getText();
  }

  async getHomePageLink(): Promise<string> {
    return element(by.css('[routerlink="/"]')).getText();
  }

  async getPersonnagesPageLink(): Promise<string> {
    return element(by.css('[routerlink="/personnages"]')).getText();
  }

  async getFavoritesPageLink(): Promise<string> {
    return element(by.css('[routerlink="/favoris"]')).getText();
  }
}

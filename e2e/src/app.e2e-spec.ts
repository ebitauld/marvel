import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display home page', async () => {
    await page.navigateTo();
    expect(await page.getHomeTitle()).toEqual('Bienvenue dans le monde de Marvel !');
  });

  it('should display personnage page', async () => {
    await page.navigateTo();
    expect(await page.getPersonnagesPageLink().getText()).toEqual('Les superheros');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
